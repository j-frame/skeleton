# J•Frame Skeleton

## Create new J•Frame Project with Composer

To get Composer, visit [getcomposer.org](https://getcomposer.org/)

```bash
composer create-project jframe/skeleton new_jframe_project
```

After project creation, goto ``new_jframe_project`` folder and run ``composer install``.
