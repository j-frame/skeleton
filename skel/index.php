<?php
/**
 * J•Frame Index File
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

use JFrame\Core\Bootstrap;

// INIT File include
require_once('JFrame' . DIRECTORY_SEPARATOR . 'init.php');

// Html output
$JFrame = $JFrame ?? null;

if ($JFrame instanceof Bootstrap) {
    /** @noinspection PhpUnhandledExceptionInspection */
    $JFrame->getCurrentResponse()->send();
} else {
    throw new \RuntimeException('JFrame not initialized...');
}
