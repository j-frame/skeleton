<?php /** @noinspection SpellCheckingInspection */

// We get the project name from the name of the path that Composer created for us.
$projectName = basename(realpath("."));
echo "Project Name $projectName taken from directory name\n";

// Copy skeleton contents into parent
cpy('./skel/', './');

echo "removing skeleton files\n";

// Then we drop the skel dir, as it contains skeleton stuff.
delTree("./skel");

// removing post create project script
exec('rm -f post_create_project.php');

// removing not necessary repository files
exec('rm -f README.md');
exec('rm -f composer.lock');
exec('rm -Rf vendor');

// We could also remove the composer.phar that the zend skeleton has here,
// but a much better choice is to remove that one from our fork directly.

echo "\033[0;32mJ•Frame - Project Creator script done...\033[0m\n";

function cpy($source, $dest): void
{
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        while($file=readdir($dir_handle)){
            if($file !== "." && $file !== ".."){
                if(is_dir($source."/".$file)){
                    if(!is_dir($dest . "/" . $file) && !mkdir($concurrentDirectory = $dest . "/" . $file) && !is_dir($concurrentDirectory)) {
                        throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                    }
                    cpy($source."/".$file, $dest."/".$file);
                } else {
                    copy($source."/".$file, $dest."/".$file);
                }
            }
        }
        closedir($dir_handle);
    } else {
        copy($source, $dest);
    }
}

/**
 * A simple recursive delTree method
 *
 * @param string $dir
 * @return bool
 */
function delTree(string $dir): bool
{
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

exit(0);
